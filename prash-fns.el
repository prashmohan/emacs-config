;;; prash-fns.el --- Custom functions for Emacs

;; Copyright (C) 2010  Prashanth Mohan

;; Author: Prashanth Mohan <prmohan@Tesla.local>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; All of this was copied from various websites and friends!

;;; Code:

(defun writeroom ()
  "Switches to a WriteRoom-like fullscreen style"
  (interactive)	
  (when (featurep 'aquamacs)
    (aquamacs-autoface-mode 0)
    ;; switch to fullscreen mode
    (aquamacs-toggle-full-frame)))

;; ***************************************************************
;; Backups go into ~/.backups
;; Overriding default function in Emacs
;; ***************************************************************
(defun make-backup-file-name (FILE)
  (let ((dirname (concat "~/.backups/emacs/"
                         (format-time-string "%y/%m/%d/"))))
    (if (not (file-exists-p dirname))
        (make-directory dirname t))
    (concat dirname (file-name-nondirectory FILE))))


;; ***************************************************************
;; cut/copy a line without marking it
;; ***************************************************************
(defadvice kill-ring-save (before slickcopy activate compile)
  "When called interactively with no active region, copy
 a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice kill-region (before slickcut activate compile)
  "When called interactively with no active region, kill
 a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defun stop-using-minibuffer ()
  "kill the minibuffer"
  (when (>= (recursion-depth) 1)
    (abort-recursive-edit)))
(add-hook 'mouse-leave-buffer-hook 'stop-using-minibuffer)

;; ***************************************************************
;; Create changelog entry for file
;; ***************************************************************
(defun changelog-entry ()
  "Add entry to ChangeLog"
  (interactive)
  (progn
    (if (equal (file-name-nondirectory (buffer-file-name)) "ChangeLog")
        (progn
          (basic-save-buffer)
          (kill-buffer "ChangeLog"))
      (progn
        (add-change-log-entry)))
;       (insert (current-time-string) "\n")))
))

;; (global-set-key "\M-n" 'changelog-entry)

;; ***************************************************************
;; Word Count
;; ***************************************************************
(defun wc ()
  "Count the words in the current buffer, show the result in the minibuffer"
  (interactive)          ; *** This is the line that you need to add
  (save-excursion 
    (save-restriction
      (widen)
      (goto-char (point-min))
      (let ((count 0))
      (while (forward-word 1)
        (setq count(1+ count)))
      (message "There are %d words in the buffer" count)))))

;; ***************************************************************
;; Pushing and poping buffer locations
;; ***************************************************************
(defvar point-stack nil)
(defun point-stack-push ()
  "Push current location and buffer info onto stack."
  (interactive)
  (message "Location marked.")
  (setq point-stack (cons (list (current-buffer) (point)) point-stack)))

(defun point-stack-pop ()
  "Pop a location off the stack and move to buffer"
  (interactive)
  (if (null point-stack)
      (message "Stack is empty.")
    (switch-to-buffer (caar point-stack))
    (goto-char (cadar point-stack))
    (setq point-stack (cdr point-stack))))

(global-set-key "\M-o" 'point-stack-push)
(global-set-key "\M-p" 'point-stack-pop)

 (defun djcb-find-file-as-root ()
  "Like `ido-find-file, but automatically edit the file with
root-privileges (using tramp/sudo), if the file is not writable by
user."
  (interactive)
  (let ((file (ido-read-file-name "Edit as root: ")))
    (unless (file-writable-p file)
      (setq file (concat "/sudo:root@localhost:" file)))
    (find-file file)))
;; or some other keybinding...
(global-set-key (kbd "C-x F") 'djcb-find-file-as-root)

(provide 'prash-fns)
;;; prash-fns.el ends here

