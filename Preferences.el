;; Copyright 2010 Prashanth Mohan
;;
;; Author: Prashanth Mohan <prashmohan@gmail.com>
;; Version: $Id: .emacs,v 0.0 2010/10/05 12:42:24 prmohan Exp $
;; Keywords: 
;; X-URL: not distributed yet

(setenv "PATH" (concat "/usr/texbin:/usr/local/bin:" (getenv "PATH")))
(add-to-list 'load-path (expand-file-name "~/.emacs.d"))
(tool-bar-mode -1)                 ;; No Toolbar!
(menu-bar-mode 1)                  ;; Yes Menubar!
(tooltip-mode 1)
(setq tramp-default-method "ssh")
(global-set-key "\C-m" 'newline-and-indent)
(setq x-select-enable-clipboard t)
(setq mac-command-modifier 'meta)
(setq mac-option-modifier 'meta)
(fset 'yes-or-no-p 'y-or-n-p)
;; See what you are typing.
(setq echo-keystrokes 0.1)
(setq require-final-newline t)
;; Open compressed files within emacs
(auto-compression-mode 1)
(xterm-mouse-mode) ;; Enable use of mouse in the terminal
(desktop-save-mode 1)
(setq desktop-restore-eager 5)
(setq el-get-verbose t)
(setq server-use-tcp t)
(server-start)
;; (set-frame-font "-*-Monaco-normal-r-*-*-15-102-120-120-c-*-iso8859-1")
(when (eq system-type 'darwin)
  ;; default Latin font (e.g. Consolas)
  (set-face-attribute 'default nil :family "Consolas")
  ;; (set-face-attribute 'default nil :family "Input")
  ;; default font size (point * 10)
  ;;
  ;; WARNING!  Depending on the default font,
  ;; if the size is not supported very well, the frame will be clipped
  ;; so that the beginning of the buffer may not be visible correctly. 
  (set-face-attribute 'default nil :height 165)
  ;; use specific font for Korean charset.
  ;; if you want to use different font size for specific charset,
  ;; add :size POINT-SIZE in the font-spec.
  (set-fontset-font t 'hangul (font-spec :name "NanumGothicCoding"))
  ;; you may want to add different for other charset in this way.
)

(setq user-mail-address "prashmohan@gmail.com")
(setq user-website "http://www.cs.berkeley.edu/~prmohan") ;; Manualy modified AUCTeX to use this variable


;; ***************************************************************
;; el-get
;; ***************************************************************

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(el-get 'sync)

;; ***************************************************************
;; ido
;; ***************************************************************
(require 'ido)
(ido-mode t)
(ido-mode 'buffer)
(setq ido-enable-flex-matching t)
(setq ibuffer-shrink-to-minimum-size t)
(setq ibuffer-always-show-last-buffer nil)
(setq ibuffer-sorting-mode 'recency)
(setq ibuffer-use-header-line t)
(global-set-key [(f12)] 'ibuffer)


;; ***************************************************************
;; recentf -- For easy access to recently visited files
;; ***************************************************************
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 500)
(setq recentf-max-menu-items 60)
(global-set-key [(meta f12)] 'recentf-open-files)


;; ***************************************************************
;; windmove - for easy navigation between frames
;; ***************************************************************
(windmove-default-keybindings)

;; ***************************************************************
;; Show line and column number
;; ***************************************************************
(require 'linum)
(line-number-mode 1)
(column-number-mode 1)  ;; Line numbers on left most column
(global-linum-mode 1)

;; ***************************************************************
;;======= Code folding =======
;; ***************************************************************

(add-hook 'python-mode-hook 'my-python-outline-hook)
; this gets called by outline to deteremine the level. Just use the length of the whitespace
(defun py-outline-level ()
  (let (buffer-invisibility-spec)
    (save-excursion
      (skip-chars-forward "    ")
      (current-column))))
; this get called after python mode is enabled
(defun my-python-outline-hook ()
  ; outline uses this regexp to find headers. I match lines with no indent and indented "class"
  ; and "def" lines.
  (setq outline-regexp "[^ \t]\\|[ \t]*\\(def\\|class\\) ")
  ; enable our level computation
  (setq outline-level 'py-outline-level)
  ; do not use their \C-c@ prefix, too hard to type. Note this overides some bindings.
  (setq outline-minor-mode-prefix "\C-t")
  ; turn on outline mode
  (outline-minor-mode t)
  ; initially hide all but the headers
  ;(hide-body)
  ; make paren matches visible
  (show-paren-mode 1)
)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default py-indent-offset 4)

;; ***************************************************************
;; Theme
;; ***************************************************************
(add-to-list 'load-path "~/.emacs.d/el-get/color-theme")
(require 'color-theme)
(color-theme-initialize)
(color-theme-clarity)

;; (require 'zenburn)
;; (zenburn)

;; ***************************************************************
;; Dired configuration
;; ***************************************************************
;; (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file)
;; (setq dired-recursive-deletes 'top)


;; ***************************************************************
;; highlight the current line; set a custom face, so we can
;; recognize from the normal marking (selection)
;; ***************************************************************
(defface hl-line '((t (:background "DarkSlateGray")))
  "Face to use for `hl-line-face'." :group 'hl-line)
(setq hl-line-face 'hl-line)
(global-hl-line-mode t) ; turn it on for all modes by default


;; ***************************************************************
;; isearch is a useful navigation mechanism. This works a little
;; better if isearch puts you at the start of the search, not the end:
;; ***************************************************************
(add-hook 'isearch-mode-end-hook 'my-goto-match-beginning)
 (defun my-goto-match-beginning ()
    (when isearch-forward (goto-char isearch-other-end)))

;; makes some movement and text commands recognize case-change as a word boundary
(add-hook 'c-mode-common-hook
               (lambda () (subword-mode 1)))


;; ***************************************************************
;; Templates
;; ***************************************************************

(require 'template)
(template-initialize)
(auto-insert-mode t)

;; ***************************************************************
;; Make Scratch buffer persistent
;; ***************************************************************
(require 'prash-scratch)


;; ***************************************************************
;; LaTeX
;; ***************************************************************
(require 'prash-latex)

;; ***************************************************************
;; org-mode
;; ***************************************************************
;; (require 'prash-org)

;; ***************************************************************
;; Emacs-Git set up
;; ***************************************************************
;; (require 'magit)
;; (require 'magit-svn)

;; ***************************************************************
;; Custom functions
;; ***************************************************************
(require 'prash-fns)

;; ***************************************************************
;; ElScreen - for virtual desktops on Emacs!
;; ***************************************************************
;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/elscreen-1.4.6"))
;; (load "elscreen" "ElScreen" t)


;; ***************************************************************
;; Markdown mode
;; ***************************************************************
(autoload 'markdown-mode "markdown-mode.el"
  "Major mode for editing Markdown files" t)
(setq auto-mode-alist
      (cons '("\\.text" . markdown-mode) auto-mode-alist))

(global-set-key (kbd "C-S-r") 'tags-apropos)

(setq-default c-basic-offset 4)
(setq c-set-style "k&r")
(setq-default c-basic-indent 4)
(setq tab-width 4)
(global-cwarn-mode 1)

(global-set-key (kbd "M-f") 'forward-word)
(global-set-key (kbd "M-b") 'backward-word)


;; (load "folding")

(require 'xcscope)
(setq cscope-do-not-update-database t)

(define-key global-map [(ctrl f3)] 'cscope-set-initial-directory)
(define-key global-map [(ctrl f4)] 'cscope-unset-initial-directory)
;; (define-key global-map (kbd "C-c C-o") 'cscope-find-this-symbol)
(global-set-key (kbd "C-x C-o") 'cscope-find-this-symbol)
(define-key global-map [(ctrl f6)] 'cscope-find-global-definition)

(define-key global-map [(ctrl f7)] 'cscope-find-global-definition-no-prompting)

(define-key global-map [(ctrl f8)] 'cscope-pop-mark)

(define-key global-map [(ctrl f9)] 'cscope-next-symbol)

(define-key global-map [(ctrl f10)] 'cscope-next-file)

(define-key global-map [(ctrl f11)] 'cscope-prev-symbol)

(define-key global-map [(ctrl f12)] 'cscope-prev-file)

(define-key global-map [(meta f9)] 'cscope-display-buffer)

(define-key global-map [(meta f10)] 'cscope-display-buffer-toggle)

(put 'narrow-to-region 'disabled nil)


(setq-default ispell-program-name "aspell")



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-command "latex -synctex=1")
 '(LaTeX-float "htb")
 '(LaTeX-indent-level 4)
 '(TeX-command-list (quote (("XeLaTeX_SyncteX" "%`xelatex --synctex=1%(mode)%' %t" TeX-run-TeX nil (latex-mode doctex-mode) :help "Run XeLaTeX") ("TeX" "%(PDF)%(tex) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil (plain-tex-mode texinfo-mode ams-tex-mode) :help "Run plain TeX") ("LaTeX" "%`%l%(mode)%' %t" TeX-run-TeX nil (latex-mode doctex-mode) :help "Run LaTeX") ("Makeinfo" "makeinfo %t" TeX-run-compile nil (texinfo-mode) :help "Run Makeinfo with Info output") ("Makeinfo HTML" "makeinfo --html %t" TeX-run-compile nil (texinfo-mode) :help "Run Makeinfo with HTML output") ("AmSTeX" "%(PDF)amstex %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil (ams-tex-mode) :help "Run AMSTeX") ("ConTeXt" "texexec --once --texutil %(execopts)%t" TeX-run-TeX nil (context-mode) :help "Run ConTeXt once") ("ConTeXt Full" "texexec %(execopts)%t" TeX-run-TeX nil (context-mode) :help "Run ConTeXt until completion") ("BibTeX" "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX") ("View" "%V" TeX-run-discard-or-function nil t :help "Run Viewer") ("Print" "%p" TeX-run-command t t :help "Print the file") ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command) ("File" "%(o?)dvips %d -o %f " TeX-run-command t t :help "Generate PostScript file") ("Index" "makeindex %s" TeX-run-command nil t :help "Create index file") ("Check" "lacheck %s" TeX-run-compile nil (latex-mode) :help "Check LaTeX file for correctness") ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document") ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files") ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files") ("Other" "" TeX-run-command t t :help "Run an arbitrary command") ("Jump to PDF" "%V" TeX-run-discard-or-function nil t :help "Run Viewer"))))
 '(TeX-modes (quote (tex-mode plain-tex-mode texinfo-mode latex-mode doctex-mode)))
 '(TeX-view-program-list (quote (("Skim" "/Applications/Skim.app/Contents/SharedSupport/displayline %n %o %b") ("Preview" "open -a Preview.app %o"))))
 '(auto-raise-tool-bar-buttons t t)
 '(auto-resize-tool-bars t t)
 '(calendar-week-start-day 1)
 '(case-fold-search t)
 '(make-backup-files nil)
 '(normal-erase-is-backspace t)
 '(org-agenda-files nil)
 '(org-agenda-ndays 7)
 '(org-agenda-repeating-timestamp-show-all nil)
 '(org-agenda-restore-windows-after-quit t)
 '(org-agenda-show-all-dates t)
 '(org-agenda-skip-deadline-if-done t)
 '(org-agenda-skip-scheduled-if-done t)
 '(org-agenda-sorting-strategy (quote ((agenda time-up priority-down tag-up) (todo tag-up))))
 '(org-agenda-start-on-weekday nil)
 '(org-agenda-todo-ignore-deadlines t)
 '(org-agenda-todo-ignore-scheduled t)
 '(org-agenda-todo-ignore-with-date t)
 '(org-agenda-window-setup (quote other-window))
 '(org-deadline-warning-days 7)
 '(org-fast-tag-selection-single-key nil)
 '(org-log-done (quote (done)))
 '(org-reverse-note-order nil)
 '(org-tags-column -78)
 '(org-tags-match-list-sublevels nil)
 '(org-time-stamp-rounding-minutes (quote (0 5)))
 '(org-use-fast-todo-selection t)
 '(safe-local-variable-values (quote ((eval ignore-errors "Write-contents-functions is a buffer-local alternative to before-save-hook" (add-hook (quote write-contents-functions) (lambda nil (delete-trailing-whitespace) nil)) (require (quote whitespace)) "Sometimes the mode needs to be toggled off and on." (whitespace-mode 0) (whitespace-mode 1)) (whitespace-line-column . 80) (whitespace-style face trailing lines-tail) (require-final-newline . t) (TeX-auto-save . t) (TeX-parse-self . t)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Browse kill ring
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(when (require 'browse-kill-ring nil 'noerror)
  (browse-kill-ring-default-keybindings))

(global-set-key "\C-cy" '(lambda ()
   (interactive)
   (popup-menu 'yank-menu)))

;; Full screen mode
(global-set-key (kbd "<f1>") 'ns-toggle-fullscreen)


;; Font size
(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C--") 'text-scale-decrease)

;; cycle through buffers
(global-set-key (kbd "<C-tab>") 'bury-buffer)

;; Change buffer window sizes
(global-set-key (kbd "<f2>") 'enlarge-window)
(global-set-key (kbd "<f3>") 'shrink-window)
(global-set-key (kbd "<f6>") 'enlarge-window-horizontally)
(global-set-key (kbd "<f7>") 'shrink-window-horizontally)


(defun my-revert-buffer()
  "revert buffer without asking for confirmation"
  (interactive "")
  (revert-buffer t t)
)

(global-set-key (kbd "C-x C-v") 'my-revert-buffer)



(add-to-list 'load-path "~/emacs.d/tiny-tools/lisp/tiny")
(add-to-list 'load-path "~/emacs.d/tiny-tools/lisp/other")

(require 'cask)
(cask-initialize)




