(TeX-add-style-hook "TEMPLATE.tex"
 (lambda ()
    (TeX-add-symbols
     "Hair")
    (TeX-run-style-hooks
     "hyperref"
     "fullpage"
     "amsmath"
     "mathptmx"
     "latex2e"
     "art11"
     "article"
     "11pt"
     "a4paper")))

