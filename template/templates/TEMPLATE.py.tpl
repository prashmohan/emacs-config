#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Summary:

Author:  prashmohan@gmail.com
         http://www.cs.berkeley.edu/~prmohan
        
License: Licensed under the CRAPL non-license -
         http://matt.might.net/articles/crapl/CRAPL-LICENSE.txt
         
         This code is provided "as-is" and is available for
         modification. If you have any questions do mail
         me.  I will _try_ to get back to you as soon as possible.
"""

from __future__ import division # Integer division returns floats
import os
import math
from collections import defaultdict
import sys

def main():
    pass

if __name__ == '__main__':
    main()
