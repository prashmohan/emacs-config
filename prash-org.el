;;; prash-org.el --- 

;; Copyright (C) 2010  Prashanth Mohan

;; Author: Prashanth Mohan <prashmohan@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(setq load-path (cons "/Users/prmohan/.emacs.d/org-7.6/lisp" load-path))
(setq load-path (cons "/Users/prmohan/.emacs.d/org-7.6/contrib/lisp" load-path))

(custom-set-variables
 '(auto-raise-tool-bar-buttons t t)
 '(auto-resize-tool-bars t t)
 '(calendar-week-start-day 1)
 '(case-fold-search t)
 '(make-backup-files nil)
 '(normal-erase-is-backspace t)
 '(org-agenda-files (quote ("/Users/prmohan/Dropbox/org/tasks.org"
                            "/Users/prmohan/Dropbox/org/journal.org"
                            "/Users/prmohan/Dropbox/org/paper-notes.org")))
 '(org-agenda-ndays 7)
 '(org-agenda-repeating-timestamp-show-all nil)
 '(org-agenda-restore-windows-after-quit t)
 '(org-agenda-show-all-dates t)
 '(org-agenda-skip-deadline-if-done t)
 '(org-agenda-skip-scheduled-if-done t)
 '(org-agenda-sorting-strategy (quote ((agenda time-up priority-down tag-up) (todo tag-up))))
 '(org-agenda-start-on-weekday nil)
 '(org-agenda-todo-ignore-deadlines t)
 '(org-agenda-todo-ignore-scheduled t)
 '(org-agenda-todo-ignore-with-date t)
 '(org-agenda-window-setup (quote other-window))
 '(org-deadline-warning-days 7)
 '(org-fast-tag-selection-single-key nil)
 '(org-log-done (quote (done)))
 '(org-reverse-note-order nil)
 '(org-tags-column -78)
 '(org-tags-match-list-sublevels nil)
 '(org-time-stamp-rounding-minutes '(0 5))
 '(org-use-fast-todo-selection t))

;; These lines only if org-mode is not part of the X/Emacs distribution.
(autoload 'org-mode "org" "Org mode" t)
(autoload 'org-diary "org" "Diary entries from Org mode")
(autoload 'org-agenda "org" "Multi-file agenda from Org mode" t)
(autoload 'org-store-link "org" "Store a link to the current location" t)
(autoload 'orgtbl-mode "org" "Org tables as a minor mode" t)
(autoload 'turn-on-orgtbl "org" "Org tables as a minor mode")

(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

(setq org-log-done nil)
(setq org-agenda-include-diary nil)
(setq org-deadline-warning-days 7)
(setq org-timeline-show-empty-dates t)
(setq org-insert-mode-line-in-empty-file t)

;; 2006-05-26  - added following line
(require 'org-install)
;; number of minutes to round time stamps to
(setq org-time-stamp-rounding-minutes '(0 5))

(autoload 'remember "remember" nil t)
(autoload 'remember-region "remember" nil t)

(setq org-directory "~/Dropbox/org/")
(setq org-default-notes-file "~/Dropbox/org/journal.org")
(org-remember-insinuate)
;; (setq remember-annotation-functions '(org-remember-annotation))
;; (setq remember-handler-functions '(org-remember-handler))
;; (add-hook 'remember-mode-hook 'org-remember-apply-template)
(define-key global-map "\C-cr" 'org-remember)
(define-key global-map [f8] 'remember)
(define-key global-map [f9] 'remember-region)

(setq org-agenda-exporter-settings
      '((ps-number-of-columns 1)
        (ps-landscape-mode t)
        (htmlize-output-type 'css)))


(setq org-agenda-custom-commands
      '(
        ("R" "Projects"   
         ((tags "PROJECTS")))
        
        ("P" "Peronsl Lists"
         ((tags "PERSONAL")))
        
        ("A" "All"
         ((agenda)
          (tags-todo "PERSONAL")
          (tags-todo "LAB")
          (tags-todo "PROJECTS")
          (tags-todo "FINANCE")
          (tags-todo "TRAVEL")))
        
        ("D" "Daily Action List"
         (
          (agenda "" ((org-agenda-ndays 1)
                      (org-agenda-sorting-strategy
                       (quote ((agenda time-up priority-down tag-up) )))
                      (org-deadline-warning-days 0)
                      ))))
        ))

(defun gtd ()
    (interactive)
    (find-file "~/Dropbox/org/tasks.org")
)
(global-set-key (kbd "C-c g") 'gtd)

(defun open-journal ()
    (interactive)
    (find-file "~/Dropbox/org/journal.org")
)
(global-set-key (kbd "C-c j") 'open-journal)

(defun paper-notes ()
    (interactive)
    (find-file "~/Dropbox/org/paper-notes.org")
)
(global-set-key (kbd "C-c p") 'paper-notes)


(add-hook 'org-agenda-mode-hook 'hl-line-mode)


(setq org-remember-templates
      '(("Todo" ?t "* TODO %^{Brief Description} %^g\n%?" "~/Dropbox/org/tasks.org" "Tasks")
        ("Journal"   ?j "** %^{Head Line} %U %^g\n%T%?"  "~/Dropbox/org/journal.org")
        ("Notes" ?p "\n\n* %^{Paper Title} %^g\n  %T\n** %?" "~/Dropbox/org/paper-notes.org")))
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

;; (autoload 'org-publish "org-publish" nil t)
;; (autoload 'org-publish "org-publish-all" nil t)
;; (autoload 'org-publish "org-publish-current-file" nil t)
;; (autoload 'org-publish "org-publish-current-project" nil t)

(load "org-publish")

(setq org-publish-project-alist
      (list
       ;; '("htmlfiles" :base-directory "~/Dropbox/org/"
	   ;;         :base-extension "org"
	   ;;         :publishing-directory "/ssh:backup:~/public_html/org/"
	   ;;         :publishing-function org-publish-org-to-html
	   ;;         :exclude "PrivatePage.org"   ;; regexp
	   ;;         :headline-levels 3
       ;;         :with-section-numbers nil
	   ;;         :table-of-contents nil
	   ;;         :auto-preamble t
	   ;;         :auto-postamble nil)
       ;; '("orgfiles" :base-directory "~/Dropbox/org/"
	   ;;         :base-extension "org"
	   ;;         :publishing-directory "/ssh:backup:~/public_html/org/"
	   ;;         :publishing-function org-publish-attachment)
       '("images" :base-directory "~/Documents/Notes/org-notes/images/"
               :base-extension "jpg\\|gif\\|png\\|pdf"
               :publishing-directory "/ssh:backup:~/public_html/notes/images/"
               :publishing-function org-publish-attachment)
       '("org-notes" :base-directory "~/Documents/Notes/org-notes/"
		       :base-extension "org"
		       :publishing-directory "/ssh:backup:~/public_html/notes/"
		       :publishing-function org-publish-org-to-html
		       :exclude "paper-reviews.org"   ;; regexp
		       :headline-levels 1
               :with-section-numbers nil
		       :table-of-contents nil
		       :auto-preamble t
               ;; :inline-images t
               ;; :include "jpg"
               :link-validation-function org-publish-validate-link
               :auto-sitemap t
               :sitemap-filename "index.org"
               :sitemap-title "Prashanth Mohan's Notes"
		       :auto-postamble t)
       )
      )

;; number of minutes to round time stamps to
(setq org-time-stamp-rounding-minutes '(5 5)) ;; Required to remove error in remember mode templates

(global-set-key "\M-p" 'org-insert-property-drawer)

(setq org-export-html-expand t)
(setq org-export-html-link-org-files-as-html t)
(provide 'prash-org)
;;; prash-org.el ends here
