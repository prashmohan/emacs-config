%%% TEMPLATE.tex.tpl --- (>>>POINT<<<)

%% Author: (>>>AUTHOR<<<)
%% Version: $Id: (>>>FILE<<<),v 0.0 (>>>VC_DATE<<<) (>>>LOGIN_NAME<<<) Exp$


\documentclass[11pt,a4paper]{article}
%%\usepackage[debugshow,final]{graphics}

%%\revision$Header: (>>>DIR<<<)(>>>FILE<<<),v 0.0 (>>>VC_DATE<<<) (>>>LOGIN_NAME<<<) Exp$
\newcommand{\Hair}{\ifmmode\mskip1mu\else\kern0.08em\fi}
\usepackage{mathptmx}
\usepackage{amsmath}

\usepackage{fullpage}
\usepackage{hyperref}


\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=true,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={},    % title
    pdfauthor={Prashanth Mohan},     % author
    pdfcreator={Prashanth Mohan},   % creator of the document
    pdfnewwindow=true,      % links in new window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=blue,          % color of internal links (change box color with linkbordercolor)
    citecolor=green,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan,           % color of external links
    pdftex
}

\frenchspacing
\begin{document}

\title{}
\date{}
\maketitle

%%%%##########################################################################




%%%%##########################################################################

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
