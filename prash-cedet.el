;;; prash-cedet.el --- CEDET configuration for my work setup

;; Copyright (C) 2011  Prashanth Mohan

;; Author: Prashanth Mohan <prashmohan@gmail.com>
;; Keywords:

;; CEDET configuration
(add-to-list 'load-path (expand-file-name "~/.emacs.d/cedet-1.1"))
(load-file "~/.emacs.d/cedet-1.1/common/cedet.el")
(semantic-mode 1)
(global-ede-mode 1)      ; Enable the Project management system
 
 
;; uncomment out one of the following 3 lines for more or less semantic features
;; (semantic-load-enable-minimum-features)
(semantic-load-enable-code-helpers) 
;; (semantic-load-enable-excessive-code-helpers) 
 
;;enable folding mode to collapse a definition into a single line
(global-semantic-tag-folding-mode)
 
;; SRecode minor mode.
(global-srecode-minor-mode 1)
 
;; (require 'semantic-ia)
;; (require 'semantic-gcc)
;; gnu global support
;; (require 'semanticdb-global)
(semanticdb-enable-gnu-global-databases 'c-mode)
(semanticdb-enable-gnu-global-databases 'c++-mode)

(require 'semantic/ia)
(require 'semantic/bovine/gcc)

 
;; ctags
;;(require 'semanticdb-ectag)
;;(semantic-load-enable-primary-exuberent-ctags-support)
 
;; customisation of modes
(defun my-cedet-hook ()
 (local-set-key [(meta return)] 'semantic-ia-complete-symbol-menu)
 (local-set-key "\C-c?" 'semantic-ia-complete-symbol)
 ;;
 (local-set-key "\C-c>" 'semantic-complete-analyze-inline)
 (local-set-key "\C-c=" 'semantic-decoration-include-visit)
 
 (local-set-key "\C-cj" 'semantic-ia-fast-jump)
 (local-set-key "\C-cb" 'semantic-mrub-switch-tags)
 (local-set-key "\C-cq" 'semantic-ia-show-doc)
 (local-set-key "\C-cs" 'semantic-ia-show-summary)
 (local-set-key "\C-cp" 'semantic-analyze-proto-impl-toggle)
 (local-set-key "\C-cr" 'semantic-symref)
 ;; for senator
 (local-set-key "\C-c\-" 'senator-fold-tag)
 (local-set-key "\C-c\+" 'senator-unfold-tag)
 )
 
;;(add-hook 'semantic-init-hooks 'my-cedet-hook)
(add-hook 'c-mode-common-hook 'my-cedet-hook)
(add-hook 'lisp-mode-hook 'my-cedet-hook)
(add-hook 'emacs-lisp-mode-hook 'my-cedet-hook)
;; (add-hook 'erlang-mode-hook 'my-cedet-hook)
 
(defun my-c-mode-cedet-hook ()
;; (local-set-key "." 'semantic-complete-self-insert)
;; (local-set-key ">" 'semantic-complete-self-insert)
 (local-set-key "\C-ct" 'eassist-switch-h-cpp)
 (local-set-key "\C-xt" 'eassist-switch-h-cpp)
 ;;(local-set-key "\C-ce" 'eassist-list-methods)
)
 
(add-hook 'c-mode-common-hook 'my-c-mode-cedet-hook)
 
;;configuring CEDET for OpenCog
;; (ede-cpp-root-project "OpenCog"
;;                 :name "OpenCog Project"
;;                :file "~/OpenCog/opencog/CMakeLists.txt"
;;                :include-path '("/")
;;                :system-include-path '("/usr/include/"
;;                                       "/usr/include/c++/4.4")
;;                ;;:spp-table '(("isUnix" . "")
;;                ;;("BOOST_TEST_DYN_LINK" . ""))
;; )

;; Load CEDET.
;; See cedet/common/cedet.info for configuration details.
;; (load-file "~/.emacs.d/cedet-1.0/common/cedet.el")

;; Enable EDE (Project Management) features
;; (global-ede-mode 1)

;; Enable EDE for a pre-existing C++ project
;; (ede-cpp-root-project "NAME" :file "~/myproject/Makefile")

;; Enabling Semantic (code-parsing, smart completion) features
;; Select one of the following:

;; * This enables the database and idle reparse engines
;; (semantic-load-enable-minimum-features)

;; * This enables some tools useful for coding, such as summary mode
;;   imenu support, and the semantic navigator
;; (semantic-load-enable-code-helpers)

;; * This enables even more coding tools such as intellisense mode
;;   decoration mode, and stickyfunc mode (plus regular code helpers)
;; (semantic-load-enable-gaudy-code-helpers)

;; * This enables the use of Exuberent ctags if you have it installed.
;;   If you use C++ templates or boost, you should NOT enable it.
;; (semantic-load-enable-all-exuberent-ctags-support)
;;   Or, use one of these two types of support.
;;   Add support for new languges only via ctags.
;; (semantic-load-enable-primary-exuberent-ctags-support)
;;   Add support for using ctags as a backup parser.
;; (semantic-load-enable-secondary-exuberent-ctags-support)

;; Enable SRecode (Template management) minor-mode.
;; (global-srecode-minor-mode 1)

;; (global-set-key (kbd "M-q") 'semantic-ia-complete-symbol-menu)
;; (global-semantic-idle-completions-mode)

(add-to-list 'load-path (expand-file-name "~/.emacs.d/jdee-2.4.1/lisp/"))
(require 'jde)
;; (require 'semantic/db-javap)
